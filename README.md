<!--
SPDX-FileCopyrightText: Andrew Hayzen <ahayzen@gmail.com>

SPDX-License-Identifier: MPL-2.0
-->

# Shell Deploy Desktop

Using this tool configures systems to have a GNOME desktop with the set of applications and configurations I prefer.

Note that it may overwrite any pre-existing data or configuration and is intended to be run after installation.

This tool targets the current latest releases of

  * Fedora Silverblue
  * Ubuntu LTS

# Usage

Execute the following commands to deploy.

```bash
./build.sh
./deploy.sh
```

If the scripts complete successfully then `systemctl reboot` your system.

## Settings

Set your custom settings in a `settings.conf` file next to `deploy.sh`, see [`src/global.sh`](./src/global.sh) and [`restore.sh`](./restore.sh) for default options which can be overriden.

As an example the following settings would enable developer and game roles to be installed, set the user name and email in places such as `git`, and when restoring from backups opt-in to restoring flatpak applications and system network connections.

```ini
INSTALL_DEVELOPER=true
INSTALL_GAMES=true

USER_EMAIL="me@example.com"
USER_NAME="Full Name"

RESTORE_FLATPAK_APPS=true
RESTORE_SYSTEM_CONNECTIONS=true
```
