#!/usr/bin/env bash

# SPDX-FileCopyrightText: Andrew Hayzen <ahayzen@gmail.com>
#
# SPDX-License-Identifier: MPL-2.0

TARGET=deploy.sh
PROJECT_FILES=(
    "src/global.sh"
    "src/utils.sh"
    "src/system/apt.sh"
    "src/system/flatpak.sh"
    "src/system/main.sh"
    "src/system/rpm_ostree.sh"
    "src/system/configure/can-background-upgrade.sh"
    "src/system/configure/dconf.sh"
    "src/system/configure/firewall.sh"
    "src/system/configure/gpg.sh"
    "src/system/configure/gtk.sh"
    "src/system/configure/luks.sh"
    "src/system/configure/main.sh"
    "src/system/configure/nvidia.sh"
    "src/system/configure/path.sh"
    "src/system/configure/swap.sh"
    "src/system/configure/systemd.sh"
    "src/gnome/apps.sh"
    "src/gnome/custom-gio-mime-default.sh"
    "src/gnome/main.sh"
    "src/gnome/session.sh"
    "src/apps/base.sh"
    "src/apps/communication.sh"
    "src/apps/games.sh"
    "src/apps/main.sh"
    "src/apps/media.sh"
    "src/developer/containers.sh"
    "src/developer/distrobox.sh"
    "src/developer/folderbox.sh"
    "src/developer/embedded.sh"
    "src/developer/flatpak.sh"
    "src/developer/git.sh"
    "src/developer/godot.sh"
    "src/developer/main.sh"
    "src/developer/projects.sh"
    "src/developer/ssh.sh"
    "src/developer/vscode.sh"
    "src/main.sh"
)

echo "#!/usr/bin/env bash" > ${TARGET}

for FILE in "${PROJECT_FILES[@]}"; do
    echo "" >> ${TARGET}
    cat "${FILE}" >> ${TARGET}
done

{
    echo ""
    echo "main"
} >> ${TARGET}
chmod +x ${TARGET}
