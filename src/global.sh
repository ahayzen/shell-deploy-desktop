# SPDX-FileCopyrightText: Andrew Hayzen <ahayzen@gmail.com>
#
# SPDX-License-Identifier: MPL-2.0

set -e
set -x

#
# Default configuration
#

INSTALL_DEVELOPER=false
INSTALL_GAMES=false
INSTALL_GAMES_STORES=false
INSTALL_GAMES_LARGE=false

if [[ "$XDG_CURRENT_DESKTOP" =~ ^GNOME ]]; then
    INSTALL_GNOME=true
else
    INSTALL_GNOME=false
fi

found=$(lspci | (grep -c -E "(VGA|3D controller).*NVIDIA" || true))
if [[ $found -eq 0 ]]; then
    INSTALL_NVIDIA=false
else
    INSTALL_NVIDIA=true
fi

USER_EMAIL=""
USER_NAME=""

#
# Load custom settings
#

SCRIPT=$(realpath "$0")
SCRIPTPATH=$(dirname "$SCRIPT")
if [ -f "$SCRIPTPATH/settings.conf" ]; then
    # shellcheck source=/dev/null
    source "$SCRIPTPATH/settings.conf"
fi

export DEBIAN_FRONTEND=noninteractive

OS_RELEASE_ID=$(grep ^ID= /etc/os-release | cut -d'=' -f2-)
OS_RELEASE_ID_LIKE=$(grep ^ID_LIKE= /etc/os-release | cut -d'=' -f2-)
OS_RELEASE_VARIANT_ID=$(grep ^VARIANT_ID= /etc/os-release | cut -d'=' -f2-)
