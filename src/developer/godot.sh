# SPDX-FileCopyrightText: Andrew Hayzen <ahayzen@gmail.com>
#
# SPDX-License-Identifier: MPL-2.0

function developer_install_godot() {
    mkdir -p ~/.local/share/com.ahayzen.folderbox/containers/godot
    tee ~/.local/share/com.ahayzen.folderbox/containers/godot/Containerfile.in <<EOF
FROM ubuntu:22.04

#include "../../common/ubuntu"

RUN apt-get update \
    && apt-get install -y \
        openjdk-11-jre-headless

COPY ./install_godot /usr/local/bin/install_godot

RUN chmod 0755 /usr/local/bin/install_godot

CMD ["/bin/bash", "--login"]
EOF

    tee ~/.local/share/com.ahayzen.folderbox/containers/godot/install_godot <<EOF_INSTALL_GODOT
#!/usr/bin/env bash

set -e

# Setup keytool
mkdir -p ~/.android

if [ ! -e ~/.android/debug.keystore ]; then
    keytool -keyalg RSA -genkeypair -alias androiddebugkey -keypass android -keystore ~/.android/debug.keystore -storepass android -dname "CN=Android Debug,O=Android,C=US" -validity 9999
fi

# Download godot
if [ ! -x ~/.local/bin/godot ]; then
    wget -O /tmp/Godot_v3.4.4.zip https://downloads.tuxfamily.org/godotengine/3.4.4/mono/Godot_v3.4.4-stable_mono_x11_64.zip
    mkdir -p ~/.local/share/godot-zip/
    unzip /tmp/Godot_v3.4.4.zip -d ~/.local/share/godot-zip/
    ln -s ~/.local/share/godot-zip/Godot_v3.4.4-stable_mono_x11_64/Godot_v3.4.4-stable_mono_x11.64 ~/.local/bin/godot
fi

# Android SDK
# We can use sdkmanager from the repos for now
if [ ! -x ~/.local/bin/sdkmanager ]; then
    # echo "Download command tools into ~/Downloads https://developer.android.com/studio/#command-tools"
    # read -r -p "Press enter when ready" _

    wget -O /tmp/sdkmanager.zip https://dl.google.com/android/repository/commandlinetools-linux-9123335_latest.zip
    mkdir -p ~/.local/share/sdkmanager-zip
    unzip /tmp/sdkmanager.zip -d ~/.local/share/sdkmanager-zip
    ln -s ~/.local/share/sdkmanager-zip/cmdline-tools/bin/sdkmanager ~/.local/bin/sdkmanager
fi

if [ ! -d ~/.local/share/androidsdk ]; then
    # distro sdkmanager sometimes need ANDROID_SDK_ROOT=~/.local/share/androidsdk
    # "cmdline-tools:latest" fails to install
    ~/.local/bin/sdkmanager --sdk_root=$(realpath ~/.local/share/androidsdk) "platform-tools" "build-tools;30.0.3" "platforms;android-31" "cmake;3.10.2.4988404" "ndk;23.2.8568313"
fi

# Configure godot

mkdir -p ~/.config/godot
GODOT_FILE=~/.config/godot/editor_settings-3.tres
touch "\$GODOT_FILE"

if ! grep -q -E "^[gd_resource]" "\$GODOT_FILE"; then
    echo "[gd_resource type=\"EditorSettings\" format=2]" >> "\$GODOT_FILE"
fi

if ! grep -q -E "^\[resource\]" "\$GODOT_FILE"; then
    sed -E -i '/^\[gd_resource/a \[resource\]' "\$GODOT_FILE"
fi

if ! grep -q -E "^export\/android\/debug_keystore " "\$GODOT_FILE"; then
    sed -E -i '/^\[resource\]/a export\/android\/debug_keystore = "\$HOME/\.android\/debug.keystore"' "\$GODOT_FILE"
fi

if ! grep -q -E "^export\/android\/debug_keystore_user " "\$GODOT_FILE"; then
    sed -E -i '/^\[resource\]/a export\/android\/debug_keystore_user = \"androiddebugkey\"' "\$GODOT_FILE"
fi

if ! grep -q -E "^export\/android\/debug_keystore_pass" "\$GODOT_FILE"; then
    sed -E -i '/^\[resource\]/a export\/android\/debug_keystore_pass = \"android\"' "\$GODOT_FILE"
fi

if ! grep -q -E "^export\/android\/android_sdk_path" "\$GODOT_FILE"; then
    sed -E -i '/^\[resource\]/a export\/android\/android_sdk_path = \"\$HOME/.local/share/androidsdk\"' "\$GODOT_FILE"
fi
EOF_INSTALL_GODOT
}
