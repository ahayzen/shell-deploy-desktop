# SPDX-FileCopyrightText: Andrew Hayzen <ahayzen@gmail.com>
#
# SPDX-License-Identifier: MPL-2.0

function developer_install_vscode() {
    developer_install_vscode_fonts
    developer_install_vscode_podman_host
    developer_install_vscode_settings

    developer_install_vscode_extensions
}

function developer_install_vscode_fonts() {
    if [ "$OS_RELEASE_ID_LIKE" == "debian" ]; then
        # Ensure that Noto Mono is available for VSCode
        sudo apt-get install -y \
            fonts-noto-mono
    fi
}

function developer_install_vscode_extension_if_not_installed() {
    # Install vscode extension if it's not installed already
    if [[ "$1" != *"$2"* ]]; then
        # If we attempt to install multiple extensions quickly, vscode can hang
        sleep 1

        flatpak run com.visualstudio.code --install-extension "$2" --force
    fi
}

function developer_install_vscode_extensions() {
    # Collect a list of installed extensions
    extensions=$(flatpak run com.visualstudio.code --list-extensions)
    # Remote Development
    developer_install_vscode_extension_if_not_installed "$extensions" "ms-vscode-remote.remote-containers"
}

function developer_install_vscode_podman_host() {
    sudo tee ~/.local/bin/podman-host <<EOF
#!/bin/bash
set -x
if [ "\$1" == "exec" ]; then
 # Remove 'exec' from \$@
 shift
 script='
     result_command="podman exec"
        for i in \$(printenv | grep "=" | grep -Ev " |\"" |
            grep -Ev "^(HOST|HOSTNAME|HOME|PATH|SHELL|USER|_)"); do

            result_command=\$result_command --env="\$i"
     done

        exec \${result_command} "\$@"
    '
 exec flatpak-spawn --host sh -c "\$script" - "\$@"
else
 exec flatpak-spawn --host podman "\$@"
fi
EOF

    # ensure this file isn't writable, vscode has corrupted it before
    sudo chmod 0500 ~/.local/bin/podman-host
    sudo chown "$USER:$USER" ~/.local/bin/podman-host
}

function developer_install_vscode_settings() {
    mkdir -p ~/.var/app/com.visualstudio.code/config/Code/User

    tee ~/.var/app/com.visualstudio.code/config/Code/User/settings.json <<EOF
{
    "cmake.configureOnOpen": true,
    "csharp.suppressDotnetInstallWarning": true,
    "dev.containers.dockerPath": "${HOME}/.local/bin/podman-host",
    "editor.fontFamily": "'Noto Mono', 'Liberation Mono', 'Source Code Pro', 'Droid Sans Mono', 'monospace', monospace",
    "editor.minimap.enabled": false,
    "editor.rulers": [79, 100],
    "editor.renderWhitespace": "boundary",
    "files.trimTrailingWhitespace": true,
    "telemetry.telemetryLevel": "error",
    "terminal.integrated.defaultProfile.linux": "bash",
    "terminal.integrated.profiles.linux": {
      "bash": {
        "path": "/usr/bin/flatpak-spawn",
        "args": ["--host", "--env=TERM=xterm-256color", "bash"]
      }
    },
    "update.mode": "none",
    "window.restoreWindows": "none",
    "window.titleBarStyle": "custom",
    "workbench.colorTheme": "Monokai",
}
EOF
}
