# SPDX-FileCopyrightText: Andrew Hayzen <ahayzen@gmail.com>
#
# SPDX-License-Identifier: MPL-2.0

function developer_install_embedded() {
    # Ensure that dialout is in /etc/group on Fedora Silverblue
    #
    # As it is only in /etc/gshadow by default
    if [ "$OS_RELEASE_ID" == "fedora" ] && [ "$OS_RELEASE_VARIANT_ID" == "silverblue" ]; then
        if ! grep -q "dialout" /etc/group; then
            grep ^dialout: /usr/lib/group | sudo tee -a /etc/group
        fi
    fi

    # Ensure we are in dialout group to access embedded boards serial port
    sudo usermod -a -G dialout "$USER"
}
