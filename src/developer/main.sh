# SPDX-FileCopyrightText: Andrew Hayzen <ahayzen@gmail.com>
#
# SPDX-License-Identifier: MPL-2.0

function developer_install() {
    developer_install_containers
    developer_install_flatpak

    developer_install_folderbox
    developer_install_distrobox
    developer_install_embedded
    developer_install_git
    developer_install_godot
    developer_install_projects
    developer_install_ssh
    developer_install_vscode
}
