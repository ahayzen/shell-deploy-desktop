# SPDX-FileCopyrightText: Andrew Hayzen <ahayzen@gmail.com>
#
# SPDX-License-Identifier: MPL-2.0

function developer_install_folderbox() {
    developer_install_folderbox_binary
    developer_install_folderbox_containers
}

function developer_install_folderbox_binary() {
    if [ ! -d ~/.var/folderbox ]; then
        # Download folderbox
        wget -O ~/.var/folderbox.zip https://github.com/ahayzen/folderbox/archive/refs/heads/main.zip
        mkdir -p ~/.var/folderbox
        unzip ~/.var/folderbox.zip -d ~/.var/folderbox

        # Install folderbox
        ~/.var/folderbox/folderbox-main/install.sh
    fi
}

function developer_install_folderbox_containers() {
    mkdir -p ~/.local/share/com.ahayzen.folderbox/containers/dev
    tee ~/.local/share/com.ahayzen.folderbox/containers/dev/Containerfile.in <<EOF
FROM ubuntu:22.04

#include "../../common/ubuntu"

#include "../../common/ubuntu-dev-cpp"
#include "../../common/ubuntu-dev-python"

#include "../../common/ubuntu-dev-analyse"

CMD ["/bin/bash", "--login"]
EOF

    # Box for using CLI tools outside of build boxes
    mkdir -p ~/.local/share/com.ahayzen.folderbox/containers/tools
    tee ~/.local/share/com.ahayzen.folderbox/containers/tools/Containerfile.in <<EOF
FROM ubuntu:22.04

#include "../../common/ubuntu"

RUN apt-get update \
    && apt-get install --yes \
        ffmpeg \
        graphicsmagick-imagemagick-compat \
        graphviz \
        nwipe \
        paperkey \
        scrcpy

CMD ["/bin/bash", "--login"]
EOF

    mkdir -p ~/.local/share/com.ahayzen.folderbox/containers/video-tools
    tee ~/.local/share/com.ahayzen.folderbox/containers/video-tools/Containerfile.in <<EOF
FROM ubuntu:22.04

#include "../../common/ubuntu"

RUN apt-get update \
    && apt-get install --yes \
        software-properties-common

# RUN curl -L https://yt-dl.org/downloads/latest/youtube-dl -o /usr/local/bin/youtube-dl \
#     && chmod a+rx /usr/local/bin/youtube-dl

RUN add-apt-repository -y ppa:yt-dlp/stable \
    && apt-get update \
    && apt-get install --yes \
        yt-dlp

RUN add-apt-repository -y ppa:m-grant-prg/utils \
    && apt-get update \
    && apt-get install --yes \
        get-iplayer

# RUN apt-get update \
#     && apt-get install -y git libtiff5-dev libtesseract-dev tesseract-ocr-eng build-essential cmake pkg-config \
#     && apt-get clean \
#     && git clone https://github.com/ruediger/VobSub2SRT.git /opt/vobsub2srt \
#     && cd /opt/vobsub2srt \
#     && git checkout 0ba6e25e078a040195d7295e860cc9064bef7c2c \
#     && ./configure \
#     && make -j\$(nproc) \
#     && make install

# RUN apt-get update \
#     && apt-get install -y curl git libtiff5-dev libtesseract-dev tesseract-ocr-eng build-essential cmake pkg-config libclang-dev \
#     && curl https://sh.rustup.rs -sSf | sh -s -- --default-toolchain stable -y \
#     && echo ". \$HOME/.cargo/env" > /etc/profile.d/99-rust.sh && . \$HOME/.cargo/env \
#     && cargo install --git https://github.com/elizagamedev/vobsubocr

CMD ["/bin/bash", "--login"]
EOF

#     mkdir -p ~/.local/share/com.ahayzen.folderbox/containers/video-tools
#     tee ~/.local/share/com.ahayzen.folderbox/containers/video-tools/Containerfile.in <<EOF
# FROM docker.io/manjarolinux/base:latest

# RUN pacman -Sy --noconfirm \
#         base-devel \
#         git \
#         yay

# RUN pacman -Sy --noconfirm \
#         rust \
#         tesseract-data-eng \
#         yt-dlp
# #    && yay -Sy --noconfirm \
# #        get_iplayer \
# #        vobsubocr

# CMD ["/bin/bash", "--login"]
# EOF
}
