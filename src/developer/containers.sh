# SPDX-FileCopyrightText: Andrew Hayzen <ahayzen@gmail.com>
#
# SPDX-License-Identifier: MPL-2.0

function developer_install_containers() {
    if [ "$INSTALL_NVIDIA" == true ]; then
        developer_install_containers_nvidia
    fi
}

# https://nvidia.github.io/nvidia-container-runtime/
# https://www.redhat.com/en/blog/how-use-gpus-containers-bare-metal-rhel-8
function developer_install_containers_nvidia() {
    if [ -x "$(command -v apt-get)" ]; then
        curl -s -L https://nvidia.github.io/nvidia-container-runtime/gpgkey | sudo apt-key add -
        # shellcheck source=/dev/null
        distribution=$(. /etc/os-release;echo "$ID$VERSION_ID")
        curl -s -L "https://nvidia.github.io/nvidia-container-runtime/$distribution/nvidia-container-runtime.list" | sudo tee /etc/apt/sources.list.d/nvidia-container-runtime.list
        sudo apt-get update

        sudo apt-get install --yes nvidia-container-toolkit
    fi

    if [ -x "$(command -v rpm-ostree)" ]; then
        # Note we use the CentOS release that matches our Fedora release
        distribution="centos8"
        curl -s -L "https://nvidia.github.io/nvidia-container-runtime/$distribution/nvidia-container-runtime.repo" | sudo tee /etc/yum.repos.d/nvidia-container-runtime.repo

        sudo rpm-ostree install --idempotent nvidia-container-toolkit
    fi

    # Allow for rootless containers
    #
    # Note if SELinux is enabled containers will need --security-opt=label=disable
    sudo sed -E -i 's/^no-cgroups = $/no-cgroups = true/g' /etc/nvidia-container-runtime/config.toml
    sudo sed -E -i 's/^debug = $/debug = "~/.local/nvidia-container-runtime.log"/g' /etc/nvidia-container-runtime/config.toml
}
