# SPDX-FileCopyrightText: Andrew Hayzen <ahayzen@gmail.com>
#
# SPDX-License-Identifier: MPL-2.0

function developer_install_flatpak() {
    sudo flatpak install --noninteractive -y flathub \
        com.visualstudio.code \
        org.gnome.Boxes
}
