# SPDX-FileCopyrightText: Andrew Hayzen <ahayzen@gmail.com>
#
# SPDX-License-Identifier: MPL-2.0

function developer_install_git() {
	mkdir -p ~/.config/git
    tee ~/.config/git/config <<EOF
[alias]
	uncommit = "reset --soft HEAD^"

[filter "lfs"]
	clean = "git-lfs clean -- %f"
	process = "git-lfs filter-process"
	required = true
	smudge = "git-lfs smudge -- %f"

[init]
	defaultBranch = "main"

[rerere]
	enable = true

[user]
	email = "${USER_EMAIL}"
	name = "${USER_NAME}"
EOF
}
