# SPDX-FileCopyrightText: Andrew Hayzen <ahayzen@gmail.com>
#
# SPDX-License-Identifier: MPL-2.0

function developer_install_projects() {
    mkdir -p ~/Projects

    # TODO: GTK 4 as well?
    if ! grep -q "file://$HOME/Projects" "$HOME/.config/gtk-3.0/bookmarks"; then
        echo "file://$HOME/Projects" >> "$HOME/.config/gtk-3.0/bookmarks"
    fi
}
