# SPDX-FileCopyrightText: Andrew Hayzen <ahayzen@gmail.com>
#
# SPDX-License-Identifier: MPL-2.0

function developer_install_distrobox() {
    if [ "$OS_RELEASE_ID_LIKE" == "debian" ]; then
        sudo apt-get install --yes \
            distrobox
    elif [ "$OS_RELEASE_ID" == "fedora" ] && [ "$OS_RELEASE_VARIANT_ID" == "silverblue" ]; then
        sudo rpm-ostree install --idempotent distrobox
    fi

    mkdir -p ~/.config/distrobox
    tee ~/.config/distrobox/distrobox.conf <<EOF
container_image_default="docker.io/library/ubuntu:22.04"
container_name_default="ubuntu-22-04"
container_init_hook="~/.config/distrobox/default_init_hook.sh"
EOF

    tee ~/.config/distrobox/default_init_hook.sh <<EOF
# Ensure manuals are expanded
if [ -x "\$(command -v unminimize)" ]; then
    yes | unminimize
fi

if [ -x "\$(command -v apt-get)" ]; then
    apt-get update
    apt-get install -y \
        bash-completion \
        byobu \
        cloc \
        git \
        git-lfs \
        nano \
        neofetch \
        man-db \
        openssh-client \
        pass \
        reuse \
        rsync \
        shellcheck
fi
EOF
    chmod 0755 ~/.config/distrobox/default_init_hook.sh
}
