# SPDX-FileCopyrightText: Andrew Hayzen <ahayzen@gmail.com>
#
# SPDX-License-Identifier: MPL-2.0

function developer_install_ssh() {
    if [ ! -f ~/.ssh/id_rsa ]; then
        # NOTE: you might want to change your password with the following
        # $ ssh-keygen -f ~/.ssh/id_rsa -p
        ssh-keygen -f ~/.ssh/id_rsa -t rsa -b 4096 -P ''
    fi
}
