# SPDX-FileCopyrightText: Andrew Hayzen <ahayzen@gmail.com>
#
# SPDX-License-Identifier: MPL-2.0

function system_install_apt() {
    system_install_apt_repositories

    system_install_apt_install
    system_install_apt_remove
    system_install_apt_timer
    system_install_apt_unattended_upgrades
}

function system_install_apt_fake_firefox() {
    # Skip if fake-package-firefox-esr is already installed
    found=$(dpkg --list-selections | (grep -c fake-package-firefox-esr || true))
    if [[ $found -gt 0 ]]; then
        return
    fi

    sudo mkdir -p /opt/fake-packages
    sudo tee /opt/fake-packages/fake-package-firefox-esr <<EOF
Section: web
Priority: optional
Standards-Version: 3.9.8.0

Package: fake-package-firefox-esr
Version: 1.0.0
Maintainer: Andrew Hayzen <ahayzen@gmail.com>
Provides: firefox-esr (= 99999.0.0)
Architecture: all
Description: Fake package
 This is a fake package to let the packaging system
 believe that this Debian package is installed.
 .
 We use the Firefox flatpak instead but still want gnome-core deb
EOF
    sudo bash -c "cd /opt/fake-packages && equivs-build /opt/fake-packages/fake-package-firefox-esr"
    sudo apt install --yes /opt/fake-packages/fake-package-firefox-esr_1.0.0_all.deb
}

function system_install_apt_install() {
    # Add policyrcd so we can have policy which doesn't autostart services
    sudo apt-get install --yes \
        policyrcd-script-zg2
    sudo tee /usr/local/sbin/policy-donotstart <<EOF
#!/bin/sh
exit 101
EOF
    sudo chmod 0755 /usr/local/sbin/policy-donotstart

    # Preseed answers to questions for apt installs
    echo "ttf-mscorefonts-installer msttcorefonts/accepted-mscorefonts-eula select true" | sudo debconf-set-selections

    sudo apt-get install --yes \
        curl \
        equivs \
        exfatprogs \
        firewalld \
        flatpak \
        podman \
        printer-driver-all \
        ttf-mscorefonts-installer \
        xfsprogs

    # Install SSH server but don't autostart
    sudo POLICYRCD=/usr/local/sbin/policy-donotstart apt-get install --yes \
        openssh-server

    system_install_apt_fake_firefox
}

function system_install_apt_remove() {
    sudo apt-get purge --auto-remove --yes \
        firefox* \
        libreoffice* \
        snapd
}

function system_install_apt_repositories() {
    # Setup repos
    if [ "$OS_RELEASE_ID" == "ubuntu" ]; then
        found=$(apt-cache policy | (grep -c multiverse || true))
        if [[ $found -eq 0 ]]; then
            sudo add-apt-repository --yes multiverse
        fi

        found=$(apt-cache policy | (grep -c restricted || true))
        if [[ $found -eq 0 ]]; then
            sudo add-apt-repository --yes restricted
        fi

        found=$(apt-cache policy | (grep -c universe || true))
        if [[ $found -eq 0 ]]; then
            sudo add-apt-repository --yes universe
        fi
    fi

    sudo apt-get update
}

function system_install_apt_timer() {
    sudo mkdir -p /etc/systemd/system/apt-daily.service.d
    sudo mkdir -p /etc/systemd/system/apt-daily.timer.d
    sudo mkdir -p /etc/systemd/system/apt-daily-upgrade.service.d

    sudo tee /etc/systemd/system/apt-daily.service.d/override.conf <<EOF
# Once apt-daily completes start apt-daily-upgrade, note that we need to use
# --no-block as apt-daily-upgrade has After=apt-daily so would cause deadlock
[Service]
# Only execute when we can run in the background
ExecCondition=/opt/ahayzen/can-background-upgrade

ExecStartPost=/usr/bin/systemctl start --no-block apt-daily-upgrade.service

# Hint that we want this to be a low priority IO task
IOSchedulingClass=idle
EOF
    sudo tee /etc/systemd/system/apt-daily.timer.d/override.conf <<EOF
[Timer]
OnCalendar=
OnBootSec=10min
OnUnitActiveSec=3hr
RandomizedDelaySec=1hr
EOF
    sudo tee /etc/systemd/system/apt-daily-upgrade.service.d/override.conf << EOF
[Service]
# Only execute when we can run in the background
ExecCondition=/opt/ahayzen/can-background-upgrade

# Run apt autoremove after upgrades have completed to ensure any are removed
# otherwise old kernels are not removed until the second time
# unattented-upgrades is run ? which causes update-manager to be launched
# with packages available to remove rather then just reboot now or later
ExecStartPost=/usr/bin/apt-get autoremove -y

# Hint that we want this to be a low priority IO task
IOSchedulingClass=idle
EOF

    sudo systemctl daemon-reload
    sudo systemctl enable --now apt-daily.timer
    sudo systemctl disable --now apt-daily-upgrade.timer
}

function system_install_apt_unattended_upgrades() {
    # Change apt 10periodic to always upgrade rather than just daily
    # otherwise if the update happens after upgrade at boot, we have to
    # wait another day.

    sudo tee /etc/apt/apt.conf.d/10periodic <<EOF
APT::Periodic::Update-Package-Lists "always";
APT::Periodic::Download-Upgradeable-Packages "always";
APT::Periodic::AutocleanInterval "0";
APT::Periodic::Unattended-Upgrade "always";
EOF

    # Enable -updates in Ubuntu
    sudo sed -E -i 's/^(\/)?(\/)?\t"\$\{distro_id\}:\$\{distro_codename\}-updates";/\t"\$\{distro_id\}:\$\{distro_codename\}-updates";/g' /etc/apt/apt.conf.d/50unattended-upgrades
    # Enable removing unused dependencies on Ubuntu - otherwise when update-manager appears it doesn't ask to restart, it prompts to remove packages
    sudo sed -E -i 's/^^(\/)?(\/)?Unattended-Upgrade::Remove-Unused-Dependencies .*/Unattended-Upgrade::Remove-Unused-Dependencies "true";/g' /etc/apt/apt.conf.d/50unattended-upgrades
}
