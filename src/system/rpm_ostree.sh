# SPDX-FileCopyrightText: Andrew Hayzen <ahayzen@gmail.com>
#
# SPDX-License-Identifier: MPL-2.0

function system_install_rpm_ostree() {
    system_install_rpm_ostree_repositories

    system_install_rpm_ostree_remove
    system_install_rpm_ostree_unattended_upgrades
}

function system_install_rpm_ostree_remove() {
    # Remove packages we don't want from base image

    # Instead of removing firefox from the base image we just mask the desktop file
    # otherwise this can break upgrades due to missing new depends like firefox-langpacks
    # https://github.com/coreos/rpm-ostree/issues/3944
    #
    # sudo rpm-ostree override remove firefox || true
    if [ -f /usr/share/applications/firefox.desktop ]; then
        cp /usr/share/applications/firefox.desktop ~/.local/share/applications/
        desktop-file-edit --set-key=NoDisplay --set-value=true ~/.local/share/applications/firefox.desktop
    fi
}

function system_install_rpm_ostree_repositories() {
    # Install extra 3rd party repositories
    # this assumes that fedora-workstation-repositories is in the image

    if [ "$INSTALL_NVIDIA" == true ]; then
        sudo sed -i '/^\[rpmfusion-nonfree-nvidia-driver\]$/,/^\[/ s/^enabled=0/enabled=1/' /etc/yum.repos.d/rpmfusion-nonfree-nvidia-driver.repo
    fi

    if [ "$INSTALL_GAMES_STORES" == true ]; then
        sudo sed -i '/^\[rpmfusion-nonfree-steam\]$/,/^\[/ s/^enabled=0/enabled=1/' /etc/yum.repos.d/rpmfusion-nonfree-steam.repo
    fi
}

function system_install_rpm_ostree_unattended_upgrades() {
    # Enable automatic updates for rpm-ostree
    sudo sed -E -i 's/^#?AutomaticUpdatePolicy=.*/AutomaticUpdatePolicy=stage/g' /etc/rpm-ostreed.conf
    sudo rpm-ostree reload

    sudo mkdir -p /etc/systemd/system/rpm-ostreed-automatic.service.d
    sudo tee /etc/systemd/system/rpm-ostreed-automatic.service.d/override.conf <<EOF
[Service]
# Only execute when we can run in the background
ExecCondition=/opt/ahayzen/can-background-upgrade

# Hint that we want this to be a low priority IO task
IOSchedulingClass=idle
EOF

    sudo mkdir -p /etc/systemd/system/rpm-ostreed-automatic.timer.d
    sudo tee /etc/systemd/system/rpm-ostreed-automatic.timer.d/override.conf <<EOF
[Timer]
OnUnitActiveSec=3hr
RandomizedDelaySec=1h
EOF

    sudo systemctl daemon-reload
    sudo systemctl enable rpm-ostreed-automatic.timer --now
}
