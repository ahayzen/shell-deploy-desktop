# SPDX-FileCopyrightText: Andrew Hayzen <ahayzen@gmail.com>
#
# SPDX-License-Identifier: MPL-2.0

function system_configure() {
    system_configure_can_background_upgrade
    system_configure_dconf
    system_configure_firewall
    system_configure_gpg
    system_configure_gtk
    system_configure_luks

    if [ "$INSTALL_NVIDIA" == true ]; then
        system_configure_nvidia
    fi

    system_configure_path
    system_configure_swap
    system_configure_systemd
}
