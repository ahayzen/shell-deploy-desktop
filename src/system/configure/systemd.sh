# SPDX-FileCopyrightText: Andrew Hayzen <ahayzen@gmail.com>
#
# SPDX-License-Identifier: MPL-2.0

function system_configure_systemd() {
    # Fedora are trying to change the timeout to 20s
    # https://pagure.io/fedora-workstation/issue/163
    #
    # THere is a pull request upstream to make this change
    # https://github.com/systemd/systemd/pull/18386
    sudo sed -E -i "s/^#?DefaultTimeoutStopSec=.*/DefaultTimeoutStopSec=15s/g" /etc/systemd/system.conf
    sudo sed -E -i "s/^#?DefaultTimeoutStopSec=.*/DefaultTimeoutStopSec=15s/g" /etc/systemd/user.conf

    if [ ! -e /lib/systemd/system/tmp.mount ] && [ -e /usr/share/systemd/tmp.mount ]; then
        sudo cp /usr/share/systemd/tmp.mount /etc/systemd/system/tmp.mount
    fi

    sudo systemctl daemon-reload
    sudo systemctl enable tmp.mount
}
