# SPDX-FileCopyrightText: Andrew Hayzen <ahayzen@gmail.com>
#
# SPDX-License-Identifier: MPL-2.0

function system_configure_luks() {
    if [ -x "$(command -v apt-get)" ]; then
        sudo apt-get install --yes \
            cryptsetup-bin
    fi

    mapfile -t devices < <( sudo blkid -o device )
    for device in "${devices[@]}"; do
        filename=${device////-}
        if sudo cryptsetup isLuks "$device"; then
            echo "LUKS: Y - $device"
            mkdir -p "$HOME/.backup-luks"
            if [ ! -f "$HOME/.backup-luks/header$filename.backup" ]; then
                sudo cryptsetup luksHeaderBackup --header-backup-file "$HOME/.backup-luks/header$filename.backup" "$device" || true
            fi
        else
            echo "LUKS: N - $device"
        fi
    done
}
