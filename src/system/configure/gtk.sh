# SPDX-FileCopyrightText: Andrew Hayzen <ahayzen@gmail.com>
#
# SPDX-License-Identifier: MPL-2.0

function system_configure_gtk() {
    mkdir -p ~/.config/gtk-4.0
    tee ~/.config/gtk-4.0/settings.ini <<EOF
[Settings]
# Enable hinting until linear layout isn't blurry
# https://gitlab.gnome.org/GNOME/gtk/-/issues/3787#note_1280247
gtk-hint-font-metrics=1
EOF
}
