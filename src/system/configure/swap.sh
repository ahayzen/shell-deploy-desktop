# SPDX-FileCopyrightText: Andrew Hayzen <ahayzen@gmail.com>
#
# SPDX-License-Identifier: MPL-2.0

function system_configure_swap() {
    if [ ! -f /var/swapfile ]; then
        sudo fallocate -l 2G /var/swapfile
        sudo chmod 0600 /var/swapfile
        sudo mkswap /var/swapfile
    fi

    sudo tee /etc/systemd/system/var-swapfile.swap <<EOF
[Unit]
Description=Swap file

[Swap]
What=/var/swapfile

[Install]
WantedBy=swap.target
EOF
    sudo systemctl daemon-reload
    sudo systemctl enable --now var-swapfile.swap
}
