# SPDX-FileCopyrightText: Andrew Hayzen <ahayzen@gmail.com>
#
# SPDX-License-Identifier: MPL-2.0

function system_configure_dconf() {
    # As per RedHat docs default values can be configured if a user profile exists
    #
    # These are then stored into /etc which is writable on Silverblue
    # compared to gschema overrides which need to be stored in /usr which is read-only
    #
    # https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/using_the_desktop_environment_in_rhel_8/configuring-gnome-at-low-level_using-the-desktop-environment-in-rhel-8#configuring-custom-default-values_configuring-gnome-at-low-level

    # Ensure we have a profile setup
    sudo tee /etc/dconf/profile/user <<EOF
user-db:user
system-db:local
system-db:site
system-db:distro
EOF

    # Ensure the folder for user drop-ins has been created
    sudo mkdir -p /etc/dconf/db/distro.d/
    sudo mkdir -p /etc/dconf/db/local.d/
    sudo mkdir -p /etc/dconf/db/site.d/
    sudo dconf update
}
