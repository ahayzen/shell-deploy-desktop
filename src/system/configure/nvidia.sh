# SPDX-FileCopyrightText: Andrew Hayzen <ahayzen@gmail.com>
#
# SPDX-License-Identifier: MPL-2.0

function system_configure_nvidia() {
    # Secure boot needs to be disabled for Nvidia on Fedora
    # https://pagure.io/fedora-workstation/issue/155
    #
    # Fedora 36 starts automatically signing akmods, we might be able to import that key
    # https://rpmfusion.org/Howto/Secure%20Boot
    #
    # Discussions on routes for a long-term solution without disabling secure boot
    # https://pagure.io/fedora-workstation/issue/155
    if [ "$OS_RELEASE_ID" == "fedora" ]; then
        SB_STATE=$(sudo mokutil --sb-state)
        if [ "$SB_STATE" == "SecureBoot enabled" ] || [ -z "$SB_STATE" ]; then
            echo "Secure boot should be disabled to install Nvidia drivers on Fedora"
            exit 1
        fi
    fi

    if [ -x "$(command -v rpm-ostree)" ]; then
        sudo rpm-ostree install --idempotent akmod-nvidia xorg-x11-drv-nvidia-cuda
        sudo rpm-ostree kargs --append=rd.driver.blacklist=nouveau --append=modprobe.blacklist=nouveau --append=nvidia-drm.modeset=1
    fi

    if [ -x "$(command -v ubuntu-drivers)" ]; then
        sudo ubuntu-drivers install nvidia
    fi
}
