# SPDX-FileCopyrightText: Andrew Hayzen <ahayzen@gmail.com>
#
# SPDX-License-Identifier: MPL-2.0

function system_configure_firewall() {
    sudo tee /etc/firewalld/zones/my-home.xml <<EOF
<?xml version="1.0" encoding="utf-8"?>
<zone target="DROP">
    <short>My Home</short>
    <description>For use in home areas. You mostly trust the other computers on networks to not harm your computer. Only selected incoming connections are accepted.</description>
    <service name="dhcpv6-client"/>
    <service name="mdns"/>
    <!-- For Media Sharing -->
    <service name="minidlna"/>
    <!-- For Nautilus file sharing -->
    <service name="samba"/>
    <service name="samba-client"/>
    <!-- For syncthing client -->
    <service name="syncthing"/>
    <!-- For Remote login -->
    <service name="ssh"/>
    <!-- For Screen Sharing -->
    <service name="vnc-server"/>
    <!-- Allow for any non system ports -->
    <!-- For File or Media Sharing which use random ports -->
    <port protocol="udp" port="1025-65535"/>
    <port protocol="tcp" port="1025-65535"/>
</zone>
EOF
    sudo sed -E -i "s/^DefaultZone=.*/DefaultZone=my-home/g" /etc/firewalld/firewalld.conf
    sudo sed -E -i "s/^LogDenied=.*/LogDenied=all/g" /etc/firewalld/firewalld.conf
    sudo systemctl restart firewalld.service
}
