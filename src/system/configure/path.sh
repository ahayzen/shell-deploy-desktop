# SPDX-FileCopyrightText: Andrew Hayzen <ahayzen@gmail.com>
#
# SPDX-License-Identifier: MPL-2.0

function system_configure_path() {
    # Ensure that local bin folder exists  and add to PATH while running in the script
    #
    # After reboot this should be picked up automatically
    mkdir -p "$HOME/.local/bin"

    export PATH="$HOME/.local/bin:$PATH"
}
