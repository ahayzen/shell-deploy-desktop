# SPDX-FileCopyrightText: Andrew Hayzen <ahayzen@gmail.com>
#
# SPDX-License-Identifier: MPL-2.0

function system_configure_gpg() {
    if [ -x "$(command -v apt-get)" ]; then
        sudo apt-get install --yes \
            gnupg \
            pcscd \
            scdaemon
    fi

    # smartcard -> pcscd -> scdaemon -> gpg-agent
    mkdir -p ~/.gnupg
    chmod 0700 ~/.gnupg
    tee ~/.gnupg/scdaemon.conf <<EOF
disable-ccid
pcsc-driver libpcsclite.so.1
EOF

    # ensure that the gpg-agent is started, otherwise nix can start it's own
    systemctl --user enable --now gpg-agent.socket
}
