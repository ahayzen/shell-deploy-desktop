# SPDX-FileCopyrightText: Andrew Hayzen <ahayzen@gmail.com>
#
# SPDX-License-Identifier: MPL-2.0

function system_install() {
    # Provision our distros to include the relevant packages

    if [ -x "$(command -v apt-get)" ]; then
        system_install_apt
    fi

    if [ -x "$(command -v rpm-ostree)" ]; then
        system_install_rpm_ostree
    fi

    # Check we have command's we'll need for later

    if [ ! -x "$(command -v curl)" ]; then
        echo "Could not find curl"
        exit 1
    fi

    if [ ! -x "$(command -v flatpak)" ]; then
        echo "Could not find flatpak"
        exit 1
    fi

    if [ ! -x "$(command -v fwupdmgr)" ]; then
        echo "Could not find fwupdmgr"
        exit 1
    fi

    # Setup common repos and configure system

    system_install_flatpak

    system_configure
}
