# SPDX-FileCopyrightText: Andrew Hayzen <ahayzen@gmail.com>
#
# SPDX-License-Identifier: MPL-2.0

function system_install_flatpak() {
    system_install_flatpak_remotes
    system_install_flatpak_timer
}

function system_install_flatpak_remotes() {
    sudo flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
    sudo flatpak remote-modify --enable --no-filter --title="Flathub" flathub
}

function system_install_flatpak_timer() {
    sudo tee /etc/systemd/system/flatpak-daily-upgrade.service <<EOF
[Unit]
Description=Daily flatpak upgrade
ConditionACPower=true
After=apt-daily.service apt-daily-upgrade.service network.target network-online.target systemd-networkd.service NetworkManager.service

[Service]
Type=oneshot
# Set XDG_CURRENT_DESKTOP otherwise certain flatpak packages won't be autodetected
# for installation, eg QGnomePlatform
Environment="XDG_CURRENT_DESKTOP=GNOME"
# Only execute when we can run in the background
ExecCondition=/opt/ahayzen/can-background-upgrade
ExecStart=flatpak update -y --noninteractive
ExecStartPost=flatpak uninstall --unused -y
KillMode=process

# Hint that we want this to be a low priority IO task
IOSchedulingClass=idle
EOF
    sudo tee /etc/systemd/system/flatpak-daily-upgrade.timer <<EOF
[Unit]
Description=Daily flatpak upgrade

[Timer]
OnBootSec=10min
OnUnitActiveSec=3hr
RandomizedDelaySec=1h

[Install]
WantedBy=timers.target
EOF
    sudo systemctl daemon-reload
    sudo systemctl enable --now flatpak-daily-upgrade.timer
}
