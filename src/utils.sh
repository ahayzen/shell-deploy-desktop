# SPDX-FileCopyrightText: Andrew Hayzen <ahayzen@gmail.com>
#
# SPDX-License-Identifier: MPL-2.0

# SUM=$( utils_file_sum /path )
function utils_file_sum() {
    sha512sum "$1" | awk '{print $1}'
}

# if utils_file_sum_changed /path "$SUM"; then
function utils_file_sum_changed() {
    NEW_SUM=$(utils_file_sum "$1")
    if [[ "$2" != "$NEW_SUM" ]]; then
        return 0
    else
        return 1
    fi
}
