# SPDX-FileCopyrightText: Andrew Hayzen <ahayzen@gmail.com>
#
# SPDX-License-Identifier: MPL-2.0

function gnome_apps_install_custom_gio_mime_default() {
    tee ~/.local/bin/custom-gio-mime-default <<EOF
#!/usr/bin/env python3

# Copyright 2020 Andrew Hayzen <ahayzen@gmail.com>
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# Python CLI tool to change the default apps for a category of mime types
# in the same way as gnome-control-center
#
# https://gitlab.gnome.org/GNOME/gnome-control-center/-/blob/dfbe1faea2705e1be2bfd11fd18d4efd7a6152b7/panels/default-apps/cc-default-apps-panel.c
# https://developer.gnome.org/gio/stable/GAppInfo.html
# https://developer.gnome.org/gio/stable/gio-Desktop-file-based-GAppInfo.html

from argparse import ArgumentParser
from gi.repository import Gio, GLib
from os import environ, path

mappings = {
    "web": {
        "content_type":
        "x-scheme-handler/http",
        "extra_type_filter":
        "text/html;application/xhtml+xml;x-scheme-handler/https",
    },
    "mail": {
        "content_type": "x-scheme-handler/mailto",
        "extra_type_filter": None,
    },
    "calendar": {
        "content_type": "text/calendar",
        "extra_type_filter": None,
    },
    "music": {
        "content_type": "audio/x-vorbis+ogg",
        "extra_type_filter": "audio/*",
    },
    "video": {
        "content_type": "video/x-ogm+ogg",
        "extra_type_filter": "video/*",
    },
    "photos": {
        "content_type": "image/jpeg",
        "extra_type_filter": "image/*",
    },
}


def get_desktop_file_as_app_info(desktop_file):
    for data_dir in environ.get("XDG_DATA_DIRS",
                                "/usr/share/applications").split(":"):
        desktop_path = path.join(data_dir, "applications", desktop_file)
        if path.exists(desktop_path):
            print("Found desktop file: %s" % desktop_path)
            return Gio.DesktopAppInfo.new_from_filename(desktop_path)


def get_all_content_types(section, app):
    extra_type_filter = mappings[section]["extra_type_filter"]
    types = [mappings[section]["content_type"]]

    if extra_type_filter is not None:
        for pattern in extra_type_filter.split(";"):
            for supported_type in app.get_supported_types():
                if GLib.pattern_match_simple(pattern, supported_type):
                    types.append(supported_type)

    return types


if __name__ == "__main__":

    def main_get(args):
        print(
            Gio.AppInfo.get_default_for_type(
                mappings[args.category]["content_type"], False).get_id())

    def main_set(args):
        app = get_desktop_file_as_app_info(args.desktop_file)

        if app is not None:
            for content_type in get_all_content_types(args.category, app):
                app.set_as_default_for_type(content_type)
        else:
            print("Could not find desktop file: %s" % args.desktop_file)

    parser = ArgumentParser(description="Change default GNOME applications")
    subparser = parser.add_subparsers()

    parser_get = subparser.add_parser("get")
    parser_get.set_defaults(func=main_get)
    parser_get.add_argument("category", type=str, choices=mappings.keys())

    parser_set = subparser.add_parser("set")
    parser_set.set_defaults(func=main_set)
    parser_set.add_argument("category", type=str, choices=mappings.keys())
    parser_set.add_argument("desktop_file", type=str)

    args = parser.parse_args()

    if hasattr(args, "func"):
        args.func(args)
    else:
        parser.print_help()
EOF

    chmod 0755 ~/.local/bin/custom-gio-mime-default
}
