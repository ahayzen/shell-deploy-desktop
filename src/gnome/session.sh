# SPDX-FileCopyrightText: Andrew Hayzen <ahayzen@gmail.com>
#
# SPDX-License-Identifier: MPL-2.0

function gnome_session_install() {
    if [ -x "$(command -v apt-get)" ]; then
        if [ "$OS_RELEASE_ID" == "ubuntu" ]; then
            gnome_session_install_fake_ubuntu_session
        fi

        gnome_session_install_apt
    fi

    gnome_session_install_flatpak
    gnome_session_install_dconf_settings

    if [ "$OS_RELEASE_ID_LIKE" == "debian" ]; then
        if [ -x "$(command -v update-alternatives)" ]; then
            gnome_session_install_alternatives_gdm
        fi
    fi

    gnome_session_install_templates
}

function gnome_session_install_alternatives_gdm() {
    sudo update-alternatives --set gdm-theme.gresource /usr/share/gnome-shell/gnome-shell-theme.gresource
}

function gnome_session_install_apt() {
    sudo apt-get install --yes \
        adwaita-icon-theme-full \
        fonts-cantarell \
        gnome-backgrounds \
        gnome-color-manager \
        gnome-remote-desktop \
        gnome-session \
        gnome-software \
        gnome-software-plugin-flatpak \
        gnome-user-share \
        network-manager-openvpn-gnome \
        network-manager-l2tp-gnome \
        network-manager-openconnect-gnome \
        network-manager-pptp-gnome \
        network-manager-ssh-gnome \
        network-manager-vpnc-gnome \
        rygel \
        ssh-askpass-gnome

    sudo apt-get purge --autoremove \
        gnome-shell-extension-prefs \
        gnome-shell-extensions \
        ubuntu-session
}

function gnome_session_install_dconf_settings() {
    sudo tee /etc/dconf/db/local.d/95_custom-desktop-settings <<EOF
# Enable week numbers in top bar calendar
[org/gnome/desktop/calendar]
show-weekdate = true


# Enable automatic timezones from geoip
[org/gnome/desktop/datetime]
automatic-timezone = true


# Disable opening Software apps automatically
[org/gnome/desktop/media-handling]
autorun-x-content-start-app = @av []


# Touchpad tweaks
[org/gnome/desktop/peripherals/touchpad]
disable-while-typing = false
tap-to-click = true

# Search providers tweaks
[org/gnome/desktop/search-providers]
enabled = ['org.gnome.Boxes.desktop', 'org.gnome.Calculator.desktop', 'org.gnome.Characters.desktop', 'org.gnome.clocks.desktop', 'org.gnome.Weather.desktop']


# Enable night light
[org/gnome/settings-daemon/plugins/color]
night-light-enabled = true

# Do not suspend when on AC after a period of time
[org/gnome/settings-daemon/plugins/power]
sleep-inactive-ac-type = 'nothing'


# Fix alphabetical ordering of app picker and disable all extensions
[org/gnome/shell]
app-picker-layout = @av []
enabled-extensions = @av []


[org/gnome/software]
# Disable automatic updates from GNOME Software as we handle them in the background (flatpak)
download-updates = false
download-updates-notify=false


# Enable location services for night light and other apps
[org/gnome/system/location]
enabled = true
EOF

    if [ "$OS_RELEASE_ID" == "fedora" ] && [ "$OS_RELEASE_VARIANT_ID" == "silverblue" ]; then
        sudo tee /etc/dconf/db/local.d/99_custom-fedora-desktop-settings <<EOF
# Set keybinding for terminal to Super + T
[org/gnome/settings-daemon/plugins/media-keys]
custom-keybindings = ['/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/']

[org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0]
binding = '<Super>t'
command = 'gnome-terminal'
name = 'Launch Terminal'
EOF
    elif [ "$OS_RELEASE_ID" == "ubuntu" ]; then
        sudo tee /etc/dconf/db/local.d/99_custom-ubuntu-desktop-settings <<EOF
# Show update-manager as soon as other updates are available (instead of weekly)
#
# This means that when /var/lib/apt/periodic/update-success-stamp is touched
# we show update-manager if there are pending updates or a reboot required
[com/ubuntu/update-notifier]
regular-auto-launch-interval = 0


# Set freedesktop as sound theme and disable input feedback sounds (eg for popups)
[org/gnome/desktop/sound]
input-feedback-sounds = false
theme-name = 'freedesktop'

# Set keybinding for terminal to Super + T
# Ubuntu already has "Launch Terminal" as a shortcut so set the keybinding
[org/gnome/settings-daemon/plugins/media-keys]
terminal = ['<Super>t']

[org/gnome/software]
# Disable updates as we handle them in the background
allow-updates = false
EOF

        sudo tee /usr/share/glib-2.0/schemas/99_custom-ubuntu-desktop-settings.gschema.override <<EOF
# ubuntu-settings sets GNOME-Greeter themes in /usr/share/glib-2.0/schemas/10_ubuntu-settings.gschema.override
# FIXME: FUTURE: until upstream supports this - LP: #1788010
#
# So until then override them back to upstream defaults.
[org.gnome.desktop.interface:GNOME-Greeter]
cursor-theme = "Adwaita"
gtk-theme = "Adwaita"
icon-theme = "Adwaita"
font-name = "Cantarell 11"
# Adobe Source Code Pro is not in Debian yet
# See https://bugs.debian.org/736681 and https://bugs.debian.org/762252
monospace-font-name = "Monospace 11"
EOF

        sudo glib-compile-schemas /usr/share/glib-2.0/schemas
    fi

    sudo dconf update
}

function gnome_session_install_flatpak() {
    sudo flatpak install --noninteractive -y flathub org.gtk.Gtk3theme.Adwaita-dark
}

function gnome_session_install_fake_ubuntu_session() {
    # Skip if fake-package-ubuntu-session is already installed
    found=$(dpkg --list-selections | (grep -c fake-package-ubuntu-session || true))
    if [[ $found -gt 0 ]]; then
        return
    fi

    sudo mkdir -p /opt/fake-packages
    sudo tee /opt/fake-packages/fake-package-ubuntu-session <<EOF
Section: gnome
Priority: optional
Standards-Version: 3.9.8.0

Package: fake-package-ubuntu-session
Version: 1.0.0
Maintainer: Andrew Hayzen <ahayzen@gmail.com>
Provides: ubuntu-session
Architecture: all
Description: Fake package
 This is a fake package to let the packaging system
 believe that this Debian package is installed.
 .
 We use gnome-session instead but still want ubuntu-desktop deb
EOF
    sudo bash -c "cd /opt/fake-packages && equivs-build /opt/fake-packages/fake-package-ubuntu-session"
    sudo apt install --yes /opt/fake-packages/fake-package-ubuntu-session_1.0.0_all.deb
}

function gnome_session_install_templates() {
    touch "$HOME/Templates/New Text Document"
}
