# SPDX-FileCopyrightText: Andrew Hayzen <ahayzen@gmail.com>
#
# SPDX-License-Identifier: MPL-2.0

function gnome_install() {
    gnome_session_install

    gnome_apps_install
}
