# SPDX-FileCopyrightText: Andrew Hayzen <ahayzen@gmail.com>
#
# SPDX-License-Identifier: MPL-2.0

function gnome_apps_install() {
    if [ -x "$(command -v apt-get)" ]; then
        gnome_apps_install_apt_install
        gnome_apps_install_apt_remove

        gnome_apps_install_apt_remove_games
        gnome_apps_install_apt_remove_media
    fi

    if [ -x "$(command -v rpm-ostree)" ]; then
        gnome_apps_install_rpm_ostree_install
    fi

    gnome_apps_install_flatpak_remove
    gnome_apps_install_flatpak

    gnome_apps_install_dconf_settings
    gnome_apps_install_default_apps

    gnome_apps_configure_flatpak
}

function gnome_apps_configure_flatpak() {
    mkdir -p ~/.var/app/org.gnome.Evolution/config/glib-2.0/settings

    EVOKEY_FILE="$HOME/.var/app/org.gnome.Evolution/config/glib-2.0/settings/keyfile"
    touch "$EVOKEY_FILE"

    # Note this needs to be kept in sync with gnome_apps_install_dconf_settings

    if ! grep -q -E "^\[org\/gnome\/evolution\/mail\]" "$EVOKEY_FILE"; then
        echo "[org/gnome/evolution/mail]" >> "$EVOKEY_FILE"
    fi

    if ! grep -q -E "^forward-style-name=.*$" "$EVOKEY_FILE"; then
        sed -E -i '/^\[org\/gnome\/evolution\/mail\]/a forward-style-name="quoted"' "$EVOKEY_FILE"
    fi

    if ! grep -q -E "^layout=.*$" "$EVOKEY_FILE"; then
        sed -E -i '/^\[org\/gnome\/evolution\/mail\]/a layout=1' "$EVOKEY_FILE"
    fi

    if ! grep -q -E "^show-to-do-bar=.*$" "$EVOKEY_FILE"; then
        sed -E -i '/^\[org\/gnome\/evolution\/mail\]/a show-to-do-bar=false' "$EVOKEY_FILE"
    fi
}

function gnome_apps_install_apt_install() {
    sudo apt-get install --yes \
        baobab \
        gnome-disk-utility \
        gnome-system-monitor \
        gnome-terminal
}

function gnome_apps_install_apt_remove() {
    sudo apt-get purge --auto-remove --yes \
        deja-dup \
        file-roller \
        gnome-contacts \
        gnome-documents \
        gnome-games \
        gnome-mahjongg \
        gnome-maps \
        gnome-music \
        gnome-todo \
        shotwell \
        thunderbird \
        transmission-gtk \
        vinagre

    # GNOME Apps that can be removed as we use flatpak
    sudo apt-get purge --auto-remove --yes \
        cheese \
        eog \
        evince \
        evolution \
        gedit \
        gnome-calculator \
        gnome-calendar \
        gnome-characters \
        gnome-clocks \
        gnome-font-viewer \
        gnome-logs \
        gnome-power-manager \
        gnome-sushi \
        gnome-sound-recorder \
        gnome-weather \
        gnucash \
        remmina \
        seahorse \
        simple-scan \
        sound-juicer
}

function gnome_apps_install_apt_remove_games() {
    sudo apt-get purge --auto-remove --yes \
        aisleriot \
        gnome-mines \
        gnome-sudoku
}

function gnome_apps_install_apt_remove_media() {
    sudo apt-get purge --auto-remove --yes \
        gimp \
        inkscape \
        rhythmbox \
        totem
}

function gnome_apps_install_default_apps() {
    gnome_apps_install_custom_gio_mime_default

    custom-gio-mime-default set calendar org.gnome.Evolution.desktop
    custom-gio-mime-default set mail org.gnome.Evolution.desktop
    custom-gio-mime-default set music org.videolan.VLC.desktop
    custom-gio-mime-default set photos org.gnome.eog.desktop
    custom-gio-mime-default set video org.videolan.VLC.desktop
    custom-gio-mime-default set web org.mozilla.firefox.desktop
}

function gnome_apps_install_flatpak() {
    sudo flatpak install --noninteractive -y flathub \
        org.gnome.Calendar \
        org.gnome.Calculator \
        org.gnome.Characters \
        org.gnome.Cheese \
        org.gnome.clocks \
        org.gnome.Connections \
        org.gnome.eog \
        org.gnome.Evince \
        org.gnome.Evolution \
        org.gnome.FileRoller \
        org.gnome.font-viewer \
        org.gnome.Logs \
        org.gnome.NautilusPreviewer \
        org.gnome.NetworkDisplays \
        org.gnome.seahorse.Application \
        org.gnome.SimpleScan \
        org.gnome.SoundJuicer \
        org.gnome.SoundRecorder \
        org.gnome.TextEditor \
        org.gnome.Weather
        # FIXME: ratbag and piper are still heavily in development
        # so versions tend to mismatch, disable for now.
        # - org.freedesktop.Piper
        # - org.gnome.Totem  # We use VLC instead
}

function gnome_apps_install_flatpak_remove_fedora() {
    found=$(flatpak info "$1" | (grep -c "Origin: fedora" || true))
    if [[ $found -gt 0 ]]; then
        sudo flatpak uninstall --noninteractive -y "$1"
    fi
}

function gnome_apps_install_flatpak_remove() {
    gnome_apps_install_flatpak_remove_fedora org.fedoraproject.MediaWriter
    gnome_apps_install_flatpak_remove_fedora org.gnome.Calculator
    gnome_apps_install_flatpak_remove_fedora org.gnome.Calendar
    gnome_apps_install_flatpak_remove_fedora org.gnome.Characters
    gnome_apps_install_flatpak_remove_fedora org.gnome.Connections
    gnome_apps_install_flatpak_remove_fedora org.gnome.Contacts
    gnome_apps_install_flatpak_remove_fedora org.gnome.Evince
    gnome_apps_install_flatpak_remove_fedora org.gnome.FileRoller
    gnome_apps_install_flatpak_remove_fedora org.gnome.Logs
    gnome_apps_install_flatpak_remove_fedora org.gnome.Maps
    gnome_apps_install_flatpak_remove_fedora org.gnome.NautilusPreviewer
    gnome_apps_install_flatpak_remove_fedora org.gnome.Screenshot
    gnome_apps_install_flatpak_remove_fedora org.gnome.Weather
    gnome_apps_install_flatpak_remove_fedora org.gnome.baobab
    gnome_apps_install_flatpak_remove_fedora org.gnome.clocks
    gnome_apps_install_flatpak_remove_fedora org.gnome.eog
    gnome_apps_install_flatpak_remove_fedora org.gnome.font-viewer
    gnome_apps_install_flatpak_remove_fedora org.gnome.gedit
    gnome_apps_install_flatpak_remove_fedora org.gnome.TextEditor
    gnome_apps_install_flatpak_remove_fedora org.gnome.Extensions

    # Try to remove the fedora runtime if it's unused
    gnome_apps_install_flatpak_remove_fedora org.fedoraproject.Platform || true
}

function gnome_apps_install_dconf_settings() {
    if [ "$INSTALL_DEVELOPER" == true ]; then
        FAVORITE_APPS="['org.mozilla.firefox.desktop', 'org.gnome.Evolution.desktop', 'org.gnome.Nautilus.desktop', 'com.spotify.Client.desktop', 'org.telegram.desktop.desktop', 'com.visualstudio.code.desktop']"
    else
        FAVORITE_APPS="['org.mozilla.firefox.desktop', 'org.gnome.Evolution.desktop', 'org.gnome.Nautilus.desktop', 'com.spotify.Client.desktop', 'org.telegram.desktop.desktop']"
    fi

    sudo tee /etc/dconf/db/local.d/95_custom-app-settings <<EOF
# Note this needs to be kept in sync with gnome_apps_configure_flatpak
[org/gnome/evolution/mail]
# use quoted forwarding, otherwise things like gmail app fail to open the original message
forward-style-name = 'quoted'
# vertical layout
layout = 1
# hide todo right side bar
show-to-do-bar = false


[org/gnome/shell]
favorite-apps = $FAVORITE_APPS


[org/gnome/terminal/legacy]
new-terminal-mode='tab'
theme-variant='dark'
EOF

    sudo dconf update
}
