# SPDX-FileCopyrightText: Andrew Hayzen <ahayzen@gmail.com>
#
# SPDX-License-Identifier: MPL-2.0

function main() {
    # Set IO scheduler to idle
    ionice -c 3 -p "$BASHPID"

    system_install

    if [ "$INSTALL_GNOME" == true ]; then
        gnome_install
    fi

    apps_install

    if [ "$INSTALL_DEVELOPER" == true ]; then
        developer_install
    fi

    echo "Reached end of script, please reboot your system!"
}
