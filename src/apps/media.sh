# SPDX-FileCopyrightText: Andrew Hayzen <ahayzen@gmail.com>
#
# SPDX-License-Identifier: MPL-2.0

function apps_install_media() {
    sudo flatpak install --noninteractive -y flathub \
        com.spotify.Client \
        org.gimp.GIMP \
        org.gimp.GIMP.Plugin.GMic//2-40 \
        org.gimp.GIMP.Plugin.Resynthesizer//2-40 \
        org.inkscape.Inkscape \
        org.videolan.VLC

    apps_install_media_configure_gimp
    apps_install_media_configure_vlc
}

function apps_install_media_configure_gimp() {
    mkdir -p "$HOME/.var/app/org.gimp.GIMP/config/GIMP/2.10"

    GIMPRC_FILE="$HOME/.var/app/org.gimp.GIMP/config/GIMP/2.10/gimprc"
    touch "$GIMPRC_FILE"

    if ! grep -q -E "^\(?theme .*$" "$GIMPRC_FILE"; then
        echo '(theme "System")' >> "$GIMPRC_FILE"
    fi

    if ! grep -q -E "^\(?icon-theme .*$" "$GIMPRC_FILE"; then
        echo '(icon-theme "Color")' >> "$GIMPRC_FILE"
    fi
}

function apps_install_media_configure_vlc() {
    sudo flatpak override \
        --own-name=org.kde.* \
        --talk-name=org.kde.StatusNotifierWatcher \
        org.videolan.VLC

    mkdir -p "$HOME/.var/app/org.videolan.VLC/config/vlc"

    VLCRC_FILE="$HOME/.var/app/org.videolan.VLC/config/vlc/vlcrc"
    touch "$VLCRC_FILE"

    if ! grep -q -E "^\[qt\].*$" "$VLCRC_FILE"; then
        echo "[qt] # Qt interface" >> "$VLCRC_FILE"
    fi

    if ! grep -q -E "^#?qt-notification=.*$" "$VLCRC_FILE"; then
        sed -E -i '/\[qt\].*/a qt-notification=0' "$VLCRC_FILE"
    fi

    if ! grep -q -E "^#?qt-privacy-ask=.*$" "$HOME/.var/app/org.videolan.VLC/config/vlc/vlcrc"; then
        sed -E -i '/\[qt\].*/a qt-privacy-ask=0' "$VLCRC_FILE"
    fi

    if ! grep -q -E "^\[core\].*$" "$VLCRC_FILE"; then
        echo "[core] # core program" >> "$VLCRC_FILE"
    fi

    if ! grep -q -E "^#?metadata-network-accessn=.*$" "$HOME/.var/app/org.videolan.VLC/config/vlc/vlcrc"; then
        sed -E -i '/\[core\].*/a metadata-network-access=1' "$VLCRC_FILE"
    fi
}
