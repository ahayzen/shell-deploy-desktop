# SPDX-FileCopyrightText: Andrew Hayzen <ahayzen@gmail.com>
#
# SPDX-License-Identifier: MPL-2.0

function apps_install_base() {
    sudo flatpak install --noninteractive -y flathub \
        com.bitwarden.desktop \
        org.libreoffice.LibreOffice \
        org.mozilla.firefox

    apps_install_base_syncthing
}

function apps_install_base_syncthing() {
    if [ "$OS_RELEASE_ID_LIKE" == "debian" ]; then
        sudo apt-get install --yes \
            syncthing
    elif [ "$OS_RELEASE_ID" == "fedora" ] && [ "$OS_RELEASE_VARIANT_ID" == "silverblue" ]; then
        sudo rpm-ostree install --idempotent syncthing
    fi

    # Manually enable the syncthing service as it might not be installed yet

    # Ensure package version of syncthing is linked to /etc
    sudo mkdir -p /etc/systemd/user
    sudo ln -f -s /usr/lib/systemd/user/syncthing.service /etc/systemd/user/syncthing.service

    # Enable the syncthing service by default
    mkdir -p "$HOME/.config/systemd/user/default.target.wants"
    sudo ln -f -s /etc/xdg/systemd/user/syncthing.service "$HOME/.config/systemd/user/default.target.wants/syncthing.service"
}
