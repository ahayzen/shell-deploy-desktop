# SPDX-FileCopyrightText: Andrew Hayzen <ahayzen@gmail.com>
#
# SPDX-License-Identifier: MPL-2.0

function apps_install() {
    apps_install_base
    apps_install_communication

    if [ "$INSTALL_GAMES" == true ]; then
        apps_install_games
    fi

    apps_install_media
}
