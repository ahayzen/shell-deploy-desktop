# SPDX-FileCopyrightText: Andrew Hayzen <ahayzen@gmail.com>
#
# SPDX-License-Identifier: MPL-2.0

function apps_install_games() {
    sudo flatpak install --noninteractive -y flathub \
        com.valvesoftware.SteamLink \
        org.gnome.Aisleriot \
        org.gnome.Chess \
        org.gnome.Mines \
        org.gnome.Reversi \
        org.gnome.Sudoku \
        org.openttd.OpenTTD

    if [ "$INSTALL_GAMES_STORES" == true ]; then
        sudo flatpak install --noninteractive -y flathub \
            com.usebottles.bottles \
            com.valvesoftware.Steam \
            com.valvesoftware.Steam.CompatibilityTool.Proton

        if [ "$OS_RELEASE_ID_LIKE" == "debian" ]; then
            sudo apt-get install --yes \
                steam-devices
        elif [ "$OS_RELEASE_ID" == "fedora" ] && [ "$OS_RELEASE_VARIANT_ID" == "silverblue" ]; then
            # udev rules for controllers may be in systemd or via a portal in the future
            #
            # Once this is done the steam-devices and rpm repo can be removed
            #
            # https://github.com/ValveSoftware/steam-devices/issues/33
            # https://github.com/systemd/systemd/issues/22681
            #
            # https://github.com/flatpak/xdg-desktop-portal/issues/536
            # https://github.com/flatpak/xdg-desktop-portal/issues/227
            sudo rpm-ostree install --idempotent steam-devices
        fi
    fi

    if [ "$INSTALL_GAMES_LARGE" == true ]; then
        sudo flatpak install --noninteractive -y flathub \
            com.play0ad.zeroad \
            net.supertuxkart.SuperTuxKart \
            org.supertuxproject.SuperTux
    fi
}
