# SPDX-FileCopyrightText: Andrew Hayzen <ahayzen@gmail.com>
#
# SPDX-License-Identifier: MPL-2.0

function apps_install_communication() {
    sudo flatpak install --noninteractive -y flathub \
        com.discordapp.Discord \
        org.telegram.desktop \
        us.zoom.Zoom

    sudo flatpak override --filesystem=home com.discordapp.Discord
}
