#!/usr/bin/env bash

# SPDX-FileCopyrightText: Andrew Hayzen <ahayzen@gmail.com>
#
# SPDX-License-Identifier: MPL-2.0

set -e
set -x

#
# Default configuration
#

RESTORE_FLATPAK_APPS=false
RESTORE_GNOME_KEYRING=false
RESTORE_GNOME_ONLINE_ACCOUNTS=false
RESTORE_SSH=false
RESTORE_STEAM=false
RESTORE_SYSTEM_CONNECTIONS=false

#
# Load custom settings
#

SCRIPT=$(realpath "$0")
SCRIPTPATH=$(dirname "$SCRIPT")
if [ -f "$SCRIPTPATH/settings.conf" ]; then
    # shellcheck source=/dev/null
    source "$SCRIPTPATH/settings.conf"
fi

function restore() {
    # Set IO scheduler to idle
    ionice -c 3 -p "$BASHPID"

    if [ ! -x "$(command -v rsync)" ]; then
        echo "rsync command not found, cannot restore"
        exit 1
    fi

    # Ask the user where to restore from
    read -r -p "Restore from: " USER_SRC
    export USER_SRC

    if [ ! -d "$USER_SRC" ]; then
        echo "Failed to find restore from target"
        exit 1
    fi

    # Bind mount readonly the restore file system, to ensure we don't delete anything
    export RESTORE_SRC=/mnt/restore-src
    restore_mount

    # Arguments to pass to rsync commands
    export RSYNC_ARGS="-avhP"

    restore_etc
    restore_home_config
    restore_home_data

    # Ensure the file system is synced
    sync

    # Unmount the bind mount
    restore_umount

    echo "Restore complete!"
    date
    echo ""
    echo "The following folders have been skipped and require manual inspection:"
    echo "Any non XDG dirs in HOME (other than ~/Archive and ~/Projects)"

    if [ "$RESTORE_SYSTEM_CONNECTIONS" == false ]; then
        echo "/etc/NetworkManager/system-connections"
    fi

    echo "$HOME/.config"
    echo "$HOME/.gnupg (only gpg import has been performed)"
    echo "$HOME/.local"

    if [ "$RESTORE_SSH" == false ]; then
        echo "$HOME/.ssh"
    fi

    if [ "$RESTORE_FLATPAK_APPS" == false ]; then
        echo "$HOME/.var/app"
    fi
}

function restore_mount() {
    if [ ! -d "$RESTORE_SRC" ]; then
        sudo mkdir -p "$RESTORE_SRC"
        sudo mount --rbind --read-only "$USER_SRC" "$RESTORE_SRC"
    else
        echo "Failed to create backup bind mount $RESTORE_SRC, as it exists already."
        exit 1
    fi
}

function restore_umount() {
    sudo umount "$RESTORE_SRC"
    sudo rm --dir "$RESTORE_SRC"
}

function restore_etc() {
    export ETC_SRC="$RESTORE_SRC/etc"

    # Restore system connections
    if [ "$RESTORE_SYSTEM_CONNECTIONS" == true ] && [ -d "$CONFIG_SRC/system-connections/" ]; then
        sudo "$(command -v rsync)" $RSYNC_ARGS "$CONFIG_SRC/system-connections/" "/etc/NetworkManager/system-connections/"
    fi
}

function restore_home_config() {
    export CONFIG_SRC="$RESTORE_SRC/home/$USER"
    export GPG_SRC="$RESTORE_SRC/gpg"

    # Restore flatpak apps
    if [ "$RESTORE_FLATPAK_APPS" == true ] && [ -d "$CONFIG_SRC/.var/app/" ]; then
        sudo "$(command -v rsync)" $RSYNC_ARGS "$CONFIG_SRC/.var/app/" "$HOME/.var/app/"
    fi

    # Restore GNOME Keyring's login.keyring
    if [ "$RESTORE_GNOME_KEYRING" == true ] && [ -f "$CONFIG_SRC/.local/share/keyring/login.keyring" ]; then
        sudo "$(command -v rsync)" $RSYNC_ARGS "$CONFIG_SRC/.local/share/keyring/login.keyring" "$HOME/.local/share/keyring/login.keyring"
    fi

    # Restore GNOME Online Accounts
    if [ "$RESTORE_GNOME_ONLINE_ACCOUNTS" == true ] && [ -d "$CONFIG_SRC/.config/goa-1.0" ]; then
        sudo "$(command -v rsync)" $RSYNC_ARGS "$CONFIG_SRC/.config/goa-1.0" "$HOME/.config/goa-1.0/"
    fi

    # Restore from keyring GnuPG if it was used
    if [ -f "$GPG_SRC/$USER-keyring.gpg" ]; then
        gpg --import-options restore --import "$GPG_SRC/$USER-keyring.gpg"
    fi

    # Restore default pass folder if it was used
    if [ -d "$CONFIG_SRC/.password-store/" ]; then
        sudo "$(command -v rsync)" $RSYNC_ARGS "$CONFIG_SRC/.password-store/" "$HOME/.password-store/"
    fi

    # Restore SSH
    if [ "$RESTORE_SSH" == true ] && [ -d "$CONFIG_SRC/.ssh/" ]; then
        sudo "$(command -v rsync)" $RSYNC_ARGS "$CONFIG_SRC/.ssh/" "$HOME/.ssh/"
    fi

    # Restore Steam
    if [ "$RESTORE_STEAM" == true ] && [ -d "$CONFIG_SRC/.steam/" ]; then
        sudo "$(command -v rsync)" $RSYNC_ARGS "$CONFIG_SRC/.steam/" "$HOME/.steam/"
    fi
}

function restore_home_data() {
    export DATA_SRC="$RESTORE_SRC/home/$USER"

    # Restore standard XDG folders
    sudo "$(command -v rsync)" $RSYNC_ARGS "$DATA_SRC/Desktop/" "$HOME/Desktop/"
    sudo "$(command -v rsync)" $RSYNC_ARGS "$DATA_SRC/Documents/" "$HOME/Documents/"
    sudo "$(command -v rsync)" $RSYNC_ARGS "$DATA_SRC/Downloads/" "$HOME/Downloads/"
    sudo "$(command -v rsync)" $RSYNC_ARGS "$DATA_SRC/Music/" "$HOME/Music/"
    sudo "$(command -v rsync)" $RSYNC_ARGS "$DATA_SRC/Pictures/" "$HOME/Pictures/"
    sudo "$(command -v rsync)" $RSYNC_ARGS "$DATA_SRC/Public/" "$HOME/Public/"
    sudo "$(command -v rsync)" $RSYNC_ARGS "$DATA_SRC/Templates/" "$HOME/Templates/"
    sudo "$(command -v rsync)" $RSYNC_ARGS "$DATA_SRC/Videos/" "$HOME/Videos/"

    # Restore extra projects folder if it exists
    if [ -d "$DATA_SRC/Projects" ]; then
        sudo "$(command -v rsync)" $RSYNC_ARGS "$DATA_SRC/Projects/" "$HOME/Projects/"
    fi

    if [ -d "$DATA_SRC/Archive" ]; then
        sudo "$(command -v rsync)" $RSYNC_ARGS "$DATA_SRC/Archive/" "$HOME/Archive/"
    fi
}

restore
