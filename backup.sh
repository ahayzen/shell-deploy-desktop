#!/usr/bin/env bash

# SPDX-FileCopyrightText: Andrew Hayzen <ahayzen@gmail.com>
#
# SPDX-License-Identifier: MPL-2.0

set -e
set -x

function backup() {
    # Set IO scheduler to idle
    ionice -c 3 -p "$BASHPID"

    if [ ! -x "$(command -v rsync)" ]; then
        echo "rsync command not found, cannot backup"
        exit 1
    fi

    # Ask the user where to store the backup
    read -r -p "Backup target root: " USER_DEST

    if [ ! -d "$USER_DEST" ]; then
        echo "Failed to find backup target"
        exit 1
    fi

    # Generate a folder for the backup and check it doesn't already exist
    DATE=$(date --iso-8601)
    export BACKUP_DEST="$USER_DEST/$DATE"
    if [ -d "$BACKUP_DEST" ]; then
        echo "Backup already exists at $BACKUP_DEST, won't overwrite"
        exit 1
    fi
    mkdir -p "$BACKUP_DEST"

    # Bind mount readonly the host file system, to ensure we don't delete anything
    export BACKUP_SRC=/mnt/backup-src
    backup_mount

    # Arguments to pass to rsync commands
    export RSYNC_ARGS="-avhP"

    backup_etc
    backup_gpg
    backup_homes
    backup_package_list

    # Ensure the file system is synced
    sync

    # Unmount bind mount
    backup_umount

    echo "Backup complete!"
    date
}

function backup_mount() {
    if [ ! -d "$BACKUP_SRC" ]; then
        sudo mkdir -p "$BACKUP_SRC/etc"
        sudo mkdir -p "$BACKUP_SRC/home"
        sudo mount --bind --read-only /etc "$BACKUP_SRC/etc"
        sudo mount --bind --read-only "/home" "$BACKUP_SRC/home"
    else
        echo "Failed to create backup bind mount $BACKUP_SRC, as it exists already."
        exit 1
    fi
}

function backup_umount() {
    sudo umount "$BACKUP_SRC/etc"
    sudo umount "$BACKUP_SRC/home"
    sudo rm --dir "$BACKUP_SRC/etc"
    sudo rm --dir "$BACKUP_SRC/home"
    sudo rm --dir "$BACKUP_SRC"
}

function backup_etc() {
    export ETC_DEST="$BACKUP_DEST/etc"
    mkdir -p "$ETC_DEST"

    # Backup all of /etc into dest
    sudo "$(command -v rsync)" $RSYNC_ARGS "$BACKUP_SRC/etc/" "$ETC_DEST"
}

function backup_gpg() {
    export GPG_DEST="$BACKUP_DEST/gpg"
    mkdir -p "$GPG_DEST"

    mapfile -t HOME_FOLDERS_ARRAY < <( ls /home )
    for HOME_FOLDER in "${HOME_FOLDERS_ARRAY[@]}"; do
        if [ -d "$BACKUP_SRC/home/$HOME_FOLDER/" ]; then
            # Backup the gpg keyring for the user
            sudo --user="$HOME_FOLDER" gpg --homedir "$BACKUP_SRC/home/$HOME_FOLDER/.gnupg/" --export-options backup -o "$GPG_DEST/$HOME_FOLDER-keyring.gpg" --export
        fi
    done
}

function backup_homes() {
    export HOME_DEST="$BACKUP_DEST/home"
    mkdir -p "$HOME_DEST"

    # Backup all of /home/$USER into dest
    sudo "$(command -v rsync)" $RSYNC_ARGS "$BACKUP_SRC/home/" "$HOME_DEST"
}

function backup_package_list() {
    export PACKAGES_DEST="$BACKUP_DEST/packages"
    mkdir -p "$PACKAGES_DEST"

    # Store apt package details
    if [ -x "$(command -v apt)" ]; then
        apt list --installed > "$PACKAGES_DEST/apt.installed"
        apt policy > "$PACKAGES_DEST/apt.policy"
    fi

    # Store flatpak package details
    if [ -x "$(command -v flatpak)" ]; then
        flatpak list --app > "$PACKAGES_DEST/flatpak.apps"
        flatpak remote-list --show-details > "$PACKAGES_DEST/flatpak.remotes"
    fi
}

backup
